<?php

namespace Xn\LogViewer;

use Xn\Admin\Admin;

trait BootExtension
{
    /**
     * {@inheritdoc}
     */
    public static function boot()
    {
        static::registerRoutes();

        Admin::extend('log-viewer', __CLASS__);
    }

    /**
     * Register routes for laravel-admin.
     *
     * @return void
     */
    protected static function registerRoutes()
    {
        parent::routes(function ($router) {
            /* @var \Illuminate\Routing\Router $router */
            $router->get('logs', 'Xn\LogViewer\LogController@index')->name('log-viewer-index');
            $router->get('logs/{file}', 'Xn\LogViewer\LogController@index')->name('log-viewer-file');
            $router->get('logs/{file}/tail', 'Xn\LogViewer\LogController@tail')->name('log-viewer-tail');
            $router->get('logs/{file}/clear', 'Xn\LogViewer\LogController@clear')->name('log-viewer-clear');
        });
    }

    /**
     * {@inheritdoc}
     */
    public static function import()
    {
        parent::createMenu('Log viewer', 'logs', 'fas fa-file-export');

        parent::createPermission('Logs', 'ext.log-viewer', 'logs*');
    }
}
