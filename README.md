Log viewer for laravel-admin
============================

## Installation

```shell
composer require xn/log-viewer -vvv
```

```shell
php artisan admin:import log-viewer
```

Open `http://your-host/admin/logs`.

License
------------
Licensed under [The MIT License (MIT)](LICENSE).
